<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Stock;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         Client::factory(15)->create();
         Stock::factory(5)->create();
    }
}
