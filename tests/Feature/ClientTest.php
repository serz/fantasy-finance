<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Client;
use LogicException;
use Tests\TestCase;

class ClientTest extends TestCase
{
    public function test_balance_cant_be_negative()
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Balance cannot be negative');
        $client = Client::first();
        $client->balance = -1;
        $client->save();
    }
}
