<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PagesTest extends TestCase
{
    public function test_about_returns_a_successful_response()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function test_clients_returns_a_successful_response()
    {
        $response = $this->get('/client');
        $response->assertStatus(200);
    }

    public function test_stocks_returns_a_successful_response()
    {
        $response = $this->get('/stock');
        $response->assertStatus(200);
    }
}
