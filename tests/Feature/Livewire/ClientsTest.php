<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Tables\Clients;
use Livewire\Livewire;
use Tests\TestCase;

class ClientsTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Clients::class);

        $component->assertStatus(200);
    }
}
