<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\AddStock;
use Livewire\Livewire;
use Tests\TestCase;

class AddStockTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(AddStock::class);

        $component->assertStatus(200);
    }
}
