<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\AddClient;
use Livewire\Livewire;
use Tests\TestCase;

class AddClientTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(AddClient::class);

        $component->assertStatus(200);
    }
}
