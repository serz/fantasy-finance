<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Tables\Stocks;
use Livewire\Livewire;
use Tests\TestCase;

class StocksTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Stocks::class);

        $component->assertStatus(200);
    }
}
