<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\PurchaseStock;
use Livewire\Livewire;
use Tests\TestCase;

class PurchaseStockTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(PurchaseStock::class);

        $component->assertStatus(200);
    }
}
