<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Tables\ClientStocks;
use App\Models\Client;
use Livewire\Livewire;
use Tests\TestCase;

class ClientStocksTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(ClientStocks::class, [
            'client' => Client::first(),
        ]);

        $component->assertStatus(200);
    }
}
