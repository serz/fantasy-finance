<?php

namespace Tests\Unit;

use App\Helpers\Money;
use Tests\TestCase;

class MoneyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_format()
    {
        $moneyFormatted = Money::format(1001.298);
        $this->assertEquals('1 001.30 EUR', $moneyFormatted);
    }
}
