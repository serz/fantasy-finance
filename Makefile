install:
	composer install
	npm install
	npm run production
	php artisan migrate:fresh --seed
	php artisan serve
up:
	php artisan serve
test:
	php artisan test
fresh:
	php artisan migrate:fresh --seed
clear:
	php artisan optimize && php artisan cache:clear
