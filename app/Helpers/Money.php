<?php

declare(strict_types=1);

namespace App\Helpers;

class Money
{
    public static function format(float $amount): string
    {
        return number_format($amount, 2, '.', ' ')
            . ' '
            . config('app.system_currency');
    }
}
