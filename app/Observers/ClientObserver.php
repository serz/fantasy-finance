<?php

namespace App\Observers;

use App\Models\Client;
use LogicException;

class ClientObserver
{
    public function creating(Client $client)
    {
        $client->balance = config('app.sign_up_bonus');
    }

    public function updating(Client $client)
    {
        if ($client->isDirty('balance') && $client->balance < 0) {
            throw new LogicException('Balance cannot be negative');
        }
    }
}
