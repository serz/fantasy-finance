<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $client_id
 * @property integer $stock_id
 * @property integer $amount
 * @property float $price
 * @property string $created_at
 * @property string $updated_at
 * @property Stock $stock
 * @property Client $client
 */
class Trade extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['client_id', 'stock_id', 'amount', 'price', 'created_at', 'updated_at'];

    public function stock(): BelongsTo
    {
        return $this->belongsTo('App\Models\Stock');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo('App\Models\Client');
    }
}
