<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer $id
 * @property string $name
 * @property float $balance
 * @property string $created_at
 * @property string $updated_at
 * @property Trade[] $trades
 */
class Client extends Model
{
    use HasFactory;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'balance', 'created_at', 'updated_at'];

    public function trades(): HasMany
    {
        return $this->hasMany('App\Models\Trade');
    }
}
