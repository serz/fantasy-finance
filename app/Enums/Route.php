<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static About()
 * @method static static Clients()
 * @method static static Stocks()
 */
final class Route extends Enum
{
    const About = 'about';
    const Clients = 'client.index';
    const Stocks = 'stock.index';
}

