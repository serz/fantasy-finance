<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class StockController extends Controller
{
    public function index(): View
    {
        return view('page.stock.index');
    }
}
