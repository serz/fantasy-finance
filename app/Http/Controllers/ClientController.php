<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Contracts\View\View;

class ClientController extends Controller
{
    public function index(): View
    {
        return view('page.client.index');
    }

    public function show(Client $client): View
    {
        return view('page.client.show', [
            'client' => $client,
        ]);
    }
}
