<?php

namespace App\Http\Livewire;

use App\Models\Stock;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AddStock extends Component
{
    public Stock $stock;

    protected array $rules = [
        'stock.name' => 'required|string|max:255',
        'stock.price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
    ];

    public function mount(): void
    {
        $this->stock = new Stock();
    }

    public function render(): View
    {
        return view('livewire.add-stock');
    }

    public function save()
    {
        $this->validate();

        $this->stock->save();

        $this->mount();
        $this->emit('refreshLivewireDatatable');
        $this->emit('hideModal', 'modal-add-stock');
    }
}
