<?php

namespace App\Http\Livewire;

use App\Models\Client;
use App\Models\Stock;
use App\Models\Trade;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use LogicException;
use Throwable;

/**
 * @property array $clients
 * @property array $stocks
 */
class PurchaseStock extends Component
{
    public Trade $trade;

    protected array $rules = [
        'trade.client_id' => 'required|integer|exists:clients,id',
        'trade.stock_id' => 'required|integer|exists:stocks,id',
        'trade.amount' => 'required|integer',
        'trade.price' => 'required|numeric',
    ];

    protected $listeners = [
        'setStockId',
    ];

    public function mount(): void
    {
        $this->trade = new Trade();
    }

    public function render(Stock $stockModel): View
    {
        $this->trade->price = optional($stockModel->query()->find($this->trade->stock_id))->price;

        return view('livewire.purchase-stock');
    }

    public function save()
    {
        $this->validate();

        // todo move to service complex logic
        try {
            DB::transaction(function () {
                /** @var Client $client */
                $client = Client::query()->findOrFail($this->trade->client_id);
                $client->update([
                    'balance' => $client->balance - $this->trade->price * $this->trade->amount,
                ]);
                $this->trade->save();
            });
        } catch (LogicException $e) {
            $this->emit('alert', $e->getMessage());
            return;
        } catch (Throwable) {
            // todo log to error tracker
            $this->emit('alert', 'Something went wrong');
            return;
        }

        $this->mount();
        $this->emit('hideModal', 'modal-purchase-stock');
        $this->emit('alert', 'Stock purchased successfully');
    }

    public function getClientsProperty(Client $clientModel): array
    {
        return $clientModel
            ->query()
            ->pluck('name', 'id')
            ->toArray();
    }

    public function getStocksProperty(Stock $stockModel): array
    {
        return $stockModel
            ->query()
            ->pluck('name', 'id')
            ->toArray();
    }

    public function setStockId(int $id): void
    {
        $this->trade->stock_id = $id;
    }
}
