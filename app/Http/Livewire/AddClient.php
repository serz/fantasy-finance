<?php

namespace App\Http\Livewire;

use App\Models\Client;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class AddClient extends Component
{
    public Client $client;

    protected array $rules = [
        'client.name' => 'required|string|max:255',
    ];

    public function mount(): void
    {
        $this->client = new Client();
    }

    public function render(): View
    {
        return view('livewire.add-client');
    }

    public function save()
    {
        $this->validate();

        $this->client->save();

        $this->mount();
        $this->emit('refreshLivewireDatatable');
        $this->emit('hideModal', 'modal-add-client');
    }
}
