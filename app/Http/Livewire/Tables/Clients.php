<?php

namespace App\Http\Livewire\Tables;

use App\Helpers\Money;
use App\Models\Client;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class Clients extends LivewireDatatable
{
    public function builder(): Builder
    {
        // todo move to repository
        return Client::query()
            ->selectRaw('coalesce(sum(amount * stocks.price) - sum(amount * trades.price), 0) as profit')
            ->leftJoin('trades', 'client_id', '=', 'clients.id')
            ->leftJoin('stocks', 'trades.stock_id', '=', 'stocks.id')
            ->groupBy('clients.id')
            ->orderByDesc('profit');
    }

    public function columns(): array
    {
        return [
            Column::name('name')
                ->searchable()
                ->unsortable(),
            NumberColumn::callback(['balance'], function ($balance) {
                return Money::format($balance);
            })->label('Balance')->unsortable(),
            ProfitColumn::name('id')
                ->label('Gain / Loss')
                ->unsortable(),
            Column::callback(['id'], function ($id): View {
                return view('page.client.actions-column', [
                    'clientId' => $id,
                ]);
            })->label('Actions')->unsortable(),
        ];
    }
}
