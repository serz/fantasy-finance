<?php

namespace App\Http\Livewire\Tables;

use App\Helpers\Money;
use App\Models\Client;
use App\Models\Trade;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

/**
 * @property string $total
 * @property string $invested
 * @property string $performance
 */
class ClientStocks extends LivewireDatatable
{
    public Client $client;

    public function builder(): Builder
    {
        $this->beforeTableSlot = 'page.client.info';

        return Trade::query()
            ->where('client_id', '=', $this->client->id);
    }

    public function columns(): array
    {
        return [
            Column::name('stock.name'),
            NumberColumn::name('amount'),
            NumberColumn::callback(['price'], function ($price): string {
                return Money::format($price);
            })->label('Purchase Price'),
            NumberColumn::callback(['stock.price'], function ($price): string {
                return Money::format($price);
            })->label('Current Price'),
            NumberColumn::callback(['amount', 'price', 'stock.price'], function ($amount, $price, $currentPrice): View {
                return view('page.client.profit-column', [
                    'profit' => $amount * $currentPrice - $amount * $price,
                ]);
            })->label('Gain / Loss'),
        ];
    }

    public function getTotalProperty()
    {
        return  Trade::query()
            ->selectRaw('sum(amount * stocks.price) - sum(amount * trades.price) as total')
            ->leftJoin('stocks', 'stock_id', '=', 'stocks.id')
            ->groupBy('client_id')
            ->value('total');
    }

    public function getInvestedProperty(): string
    {
        return Trade::query()
            ->selectRaw('sum(amount * price) as invested')
            ->where('client_id', '=', $this->client->id)
            ->groupBy('client_id')
            ->value('invested');
    }

    public function getPerformanceProperty(): float
    {
        return round($this->total * 100 / $this->invested, 2);
    }
}
