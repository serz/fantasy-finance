<?php

namespace App\Http\Livewire\Tables;

use App\Models\Stock;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class Stocks extends LivewireDatatable
{
    public function builder(): Builder
    {
        return Stock::query()
            ->orderBy('price');
    }

    public function columns(): array
    {
        return [
            Column::name('name')
                ->unsortable(),
            Column::name('price')
                ->label('Price (click to edit)')
                ->editable()
                ->unsortable(),
            DateColumn::name('updated_at'),
            Column::callback(['id'], function ($id): View {
                return view('page.stock.actions-column', [
                    'stockId' => $id,
                ]);
            })->label('Actions')->unsortable(),
        ];
    }
}
