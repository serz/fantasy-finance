<?php

declare(strict_types=1);

namespace App\Http\Livewire\Tables;

use Illuminate\Contracts\View\View;
use Mediconesystems\LivewireDatatables\NumberColumn;

class ProfitColumn extends NumberColumn
{
    public function __construct()
    {
        parent::__construct();
        $this->callback = function ($value, $row): View {
            return view('page.client.profit-column', [
                'profit' => (float)$row->profit,
            ]);
        };
    }
}
