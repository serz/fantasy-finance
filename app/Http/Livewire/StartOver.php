<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Artisan;
use Livewire\Component;

class StartOver extends Component
{
    public function render(): View
    {
        return view('livewire.start-over');
    }

    public function refreshDatabase()
    {
        Artisan::call('migrate:fresh', [
            '--seed' => true,
        ]);
        $this->emit('alert', 'Database refreshed successfully');
    }
}
