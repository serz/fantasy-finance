[Demo App Link](http://phplaravel-677903-2655254.cloudwaysapps.com/)

### Installation
- cp .env.example .env
- change mysql credentials to local
- run quick command `make install`
- should work 🤞 at http://127.0.0.1:8000 (or other port if this one is in use already)

### Quick Commands
Start the application:
```shell
make up
```

Run tests:
```shell
make test
```

Rollup migrations & seed test data:
```shell
make fresh
```
### Entity-relationship Diagram
![erd.png](erd.png)
