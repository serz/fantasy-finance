<?php
/**
 * @var PurchaseStock $this
 */

use App\Http\Livewire\PurchaseStock;

?>

<div class="modal-dialog" role="document">
    <form class="modal-content" wire:submit.prevent="save">
        <div class="modal-header">
            <h5 class="modal-title">Purchase a stock</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>

        <div class="modal-body">
            <div class="form-group">
                <label>
                    Client
                    <span class="text-danger">*</span>
                </label>
                @include('layout.select-options', [
                    'modelProperty' => 'trade.client_id',
                    'options' => $this->clients,
                ])
            </div>

            <div class="form-group">
                <label>
                    Stock
                    <span class="text-danger">*</span>
                </label>
                @include('layout.select-options', [
                    'modelProperty' => 'trade.stock_id',
                    'options' => $this->stocks,
                ])
            </div>

            <div class="form-group">
                <label>
                    Volume
                    <span class="text-danger">*</span>
                </label>
                <input type="number" min="0" class="form-control" wire:model="trade.amount" required>
            </div>

            <p class="font-bold">
                Price:
                {{ $this->trade->price && $this->trade->amount
                    ? $this->trade->price * $this->trade->amount
                    : 0 }}
                {{ config('app.system_currency') }}
            </p>

            @include('layout.alert')
        </div>

        <div class="modal-footer">
            <button type="button"
                    class="bg-white border border-gray-300 hover:bg-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800"
                    data-dismiss="modal"
            >
                Close
            </button>
            <button
                type="submit"
                class="float-right text-white bg-purple-700 hover:bg-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700"
            >
                Purchase
            </button>
        </div>
    </form>
</div>
