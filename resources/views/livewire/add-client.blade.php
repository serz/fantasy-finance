<div class="modal-dialog" role="document">
    <form class="modal-content" wire:submit.prevent="save">
        <div class="modal-header">
            <h5 class="modal-title">Create a new client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>

        <div class="modal-body">
            <div class="form-group">
                <label>
                    Name
                    <span class="text-danger">*</span>
                </label>
                <input type="text" class="form-control" wire:model.defer="client.name" required>
            </div>

            @include('layout.alert')
        </div>

        <div class="modal-footer">
            <button type="button"
                    class="bg-white border border-gray-300 hover:bg-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800"
                    data-dismiss="modal"
            >
                Close
            </button>
            <button
                type="submit"
                class="float-right text-white bg-purple-700 hover:bg-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700"
            >
                Add
            </button>
        </div>
    </form>
</div>
