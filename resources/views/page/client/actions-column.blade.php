<?php
/**
 * @var int $clientId
 */
?>

<a href="{{ route('client.show', $clientId) }}" class="cursor-pointer mr-4">
    <i class="far fa-eye"></i> View stocks
</a>
