<?php
/**
 * @var float $profit
 */

if ($profit > 0) {
    $textColor = 'green';
} elseif ($profit < 0) {
    $textColor = 'red';
} else {
    $textColor = 'blue';
}
?>

<span class="text-{{ $textColor }}-800">
    {{ \App\Helpers\Money::format($profit) }}
</span>
