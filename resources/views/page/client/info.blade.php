<?php
/**
 * @var ClientStocks $this
 * @var Client $client
 */

use App\Http\Livewire\Tables\ClientStocks;
use App\Models\Client;

?>

<p>
    Total: {{ \App\Helpers\Money::format($this->total) }}
</p>
<p>
    Invested: {{ \App\Helpers\Money::format($this->invested) }}
</p>
<p>
    Performance: {{ $this->performance }} %
</p>
<p>
    Cash Balance: {{ \App\Helpers\Money::format($client->balance) }}
</p>
