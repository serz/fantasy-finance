@extends('layout.layout')

@section('content')
<main class="mt-4 mx-auto px-4 sm:px-6 lg:px-8">
    <div class="sm:text-center lg:text-left">
        <h1 class="text-3xl tracking-tight font-extrabold text-gray-900 sm:text-4xl md:text-5xl mb-4">
            Clients

            <button
                type="button"
                class="float-right text-white bg-purple-700 hover:bg-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700"
                data-toggle="modal"
                data-target="#modal-add-client"
            >
                <i class="fa fa-plus"></i> Add client
            </button>
        </h1>

        <livewire:tables.clients />

        <div class="modal fade"
             id="modal-add-client"
             tabindex="-1"
             role="dialog"
             aria-labelledby="modal-add-client"
             aria-hidden="true"
        >
            <livewire:add-client />
        </div>
    </div>
</main>
@stop
