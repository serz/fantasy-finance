<?php
/**
 * @var Client $client
 */

use App\Models\Client;
?>

@extends('layout.layout')

@section('content')
<main class="mt-4 mx-auto px-4 sm:px-6 lg:px-8">
    <div class="sm:text-center lg:text-left">
        <h1 class="text-2xl tracking-tight font-extrabold text-gray-900 sm:text-3xl md:text-4xl mb-4">
            {{ $client->name }} stocks
        </h1>

        @if($client->trades()->count())
            <livewire:tables.client-stocks :client="$client" />
        @else
            <p>
                No stocks purchased
            </p>
        @endif
    </div>
</main>
@stop
