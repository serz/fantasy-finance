<?php
/**
 * @var int $stockId
 */
?>

<span class="cursor-pointer text-red-800 mr-4"
      onclick="confirm(`Are you sure you want to delete stock?`) || event.stopImmediatePropagation()"
      wire:click="delete({{ $stockId }})"
>
    <i class="far fa-trash-alt"></i> Delete
</span>

<span class="cursor-pointer text-green-800"
      data-toggle="modal"
      data-target="#modal-purchase-stock"
      wire:click="$emit('setStockId', {{ $stockId }})"
>
    <i class="fa fa-money-check-dollar"></i> Purchase
</span>

<div class="modal fade"
     id="modal-purchase-stock"
     tabindex="-1"
     role="dialog"
     aria-labelledby="modal-purchase-stock"
     aria-hidden="true"
>
    <livewire:purchase-stock />
</div>
