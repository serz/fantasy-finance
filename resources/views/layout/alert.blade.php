@if(!$errors->isEmpty())
    <div class="alert alert-danger" role="alert">
        <div class="alert-icon"><i class="flaticon-warning"></i></div>
        <div class="alert-text">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-danger" role="alert">
        <div class="alert-icon"><i class="flaticon-exclamation-2"></i></div>
        <div class="alert-text">
            @if(is_array($message))
                @foreach ($message as $message)
                    {{ $message }}<br>
                @endforeach
            @else
                {{ $message }}
            @endif
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif
