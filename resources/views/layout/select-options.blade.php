<?php
/**
 * @var string $modelProperty
 * @var array $options
 */
?>

<div class="input-group">
    <select class="form-select w-full form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0"
            wire:model="{{ $modelProperty }}"
            required
    >
        <option value="">Select</option>
        @foreach($options as $key => $option)
            <option value="{{ $key }}">
                {{ $option }}
            </option>
        @endforeach
    </select>
</div>
